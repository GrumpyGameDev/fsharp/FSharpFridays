﻿namespace AceyDucey

open System
//AceyDucey
//The dealer deals two cards face up
//Player bets on whether or not a third card will be between the first two
//Player starts with $100
//Player loses when they are out of money

//TODO: model a player with money and round state
//TODO: display initial state of round
//TODO: accept input from player whether or not to bed
//TODO: accept amount of bet from player
//TODO: evaluate round outcome

type CardSuit = 
    | Clubs 
    | Hearts 
    | Spades 
    | Diamonds
type CardFace = 
    | Deuce 
    | Three 
    | Four 
    | Five 
    | Six 
    | Seven 
    | Eight 
    | Nine 
    | Ten 
    | Jack 
    | Queen 
    | King 
    | Ace
type Card = CardFace * CardSuit
type Deck = Card list
type Round = 
    {
        FirstCard  : Card
        SecondCard : Card
        ThirdCard  : Card
    }

//player states:
// Initial/Resolved - No Bet, No Round
// Dealt - No Bet, Some Round
// Bet Placed - Some Bet, Some Round
type PlayerState =
    | Resolved
    | Dealt of Round
    | BetPlaced of double * Round
type Player =
    {
        Money : double 
        State : PlayerState
    }

module CardFace =
    let GetValue (face:CardFace) : int =
        match face with
        | Deuce ->  2
        | Three ->  3
        | Four  ->  4
        | Five  ->  5
        | Six   ->  6
        | Seven ->  7
        | Eight ->  8
        | Nine  ->  9
        | Ten   -> 10
        | Jack  -> 11
        | Queen -> 12
        | King  -> 13
        | Ace   -> 14

[<AutoOpen>]
module CardFaceActivePattern =
    let (|FIRST|SECOND|NEITHER|) (first:CardFace, second:CardFace) =
        let firstValue = first |> CardFace.GetValue
        let secondValue = second |> CardFace.GetValue
        if firstValue < secondValue then 
            FIRST
        elif secondValue < firstValue then
            SECOND
        else
            NEITHER

module Deck =
    let Create () : Card list =
        let makeSuitedCardList (face:CardFace) : Card list =
            [ Clubs; Hearts; Spades; Diamonds]
            |> List.map (fun suit -> face, suit)
        [Ace ; Deuce ; Three ; Four ; Five ; Six ; Seven ; Eight ; Nine ; Ten ; Jack ; Queen ; King]
        |> List.map makeSuitedCardList 
        |> List.reduce (@)

    let Shuffle (random:Random) (deck:Deck) : Deck =
        deck
        |> List.sortBy (fun _ -> random.Next())

    let DealRound (deck:Deck) : Round * Deck =
        ({
            FirstCard = deck.Head
            SecondCard = deck.Tail.Head
            ThirdCard = deck.Tail.Tail.Head
        }, deck.Tail.Tail.Tail)

module Round =
    let IsWinner (round:Round) : bool =
        match (round.FirstCard |> fst, round.SecondCard |> fst) with
        | NEITHER ->
            false
        | FIRST ->
            let low = round.FirstCard |> fst |> CardFace.GetValue
            let high = round.SecondCard |> fst |> CardFace.GetValue
            let value = round.ThirdCard |> fst |> CardFace.GetValue
            (value > low && value < high)
        | SECOND ->
            let low = round.SecondCard |> fst |> CardFace.GetValue
            let high = round.FirstCard |> fst |> CardFace.GetValue
            let value = round.ThirdCard |> fst |> CardFace.GetValue
            (value > low && value < high)

module Player =
    let Create () : Player =
        {
            Money = 100.0
            State = Resolved
        }

    let Deal (random:Random) (player:Player) : Player =
        match player.State with
        | Resolved ->
            {
                Money = player.Money
                State = 
                    Deck.Create() 
                    |> Deck.Shuffle random 
                    |> Deck.DealRound 
                    |> fst 
                    |> Dealt
            }
        | _ -> player

    let Bet (amount:double) (player:Player) : Player =
        match player.State with
        | Dealt round ->
            if amount < 0.0 then
                player
            elif amount >= player.Money then
                {player with 
                    State = BetPlaced (player.Money, round)
                    Money = 0.0}
            else
                {player with
                    State = BetPlaced (amount, round)
                    Money = player.Money - amount}
        | _ -> 
            player

    let Resolve (player:Player) : Player =
        match player.State with
        | BetPlaced (amount, round) ->
            if round |> Round.IsWinner then
                {player with 
                    Money = player.Money + (amount * 2.0)
                    State = Resolved}
            else
                {player with 
                    State = Resolved}
        | _ ->
            player
