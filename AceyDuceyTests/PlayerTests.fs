﻿module PlayerTests

open NUnit.Framework
open AceyDucey
open System

[<Test>]
let ``Create.It initializes a new player in the resolved state.`` () =
    let expected = 
        {
            Money = 100.0
            State = Resolved
        }
    let actual = Player.Create()
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Deal.It deals a round for a player in the resolved state.`` () =
    let player = Player.Create()
    let random = Random(999)
    let expected = 
        {
            Money = 100.0
            State = Dealt 
                {
                    FirstCard = (Nine, Clubs)
                    SecondCard = (Six, Diamonds)
                    ThirdCard = (Nine, Hearts)
                }
        }
    let actual = player |> Player.Deal random
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Deal.It does nothing when the player is in the dealt state.`` () =
    let random = Random(999)
    let player = Player.Create() |> Player.Deal random
    let expected = 
        player
    let actual = 
        player 
        |> Player.Deal random
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Deal.It does nothing when the player is in the bet placed state.`` () =
    let random = Random(999)
    let player = Player.Create() |> Player.Deal random |> Player.Bet 0.0
    let expected = 
        player
    let actual = 
        player 
        |> Player.Deal random
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Bet.It does nothing when the player is in the resolved state.`` () =
    let player = Player.Create()
    let expected = 
        player
    let actual = 
        player 
        |> Player.Bet 1.0
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Bet.It does nothing when the player is in the bet placed state.`` () =
    let random = Random(999)
    let player = Player.Create() |> Player.Deal random |> Player.Bet 0.0
    let expected = 
        player
    let actual = 
        player 
        |> Player.Bet 1.0
    Assert.AreEqual(expected, actual)


[<Test>]
let ``Bet.It does not set the bet for the player when the given amount is negative when the player is in the dealt state.`` () =
    let random = Random()
    let player = Player.Create() |> Player.Deal random
    let amount = -10.0
    let expected = 
        player
    let actual =
        player
        |> Player.Bet amount
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Bet.It sets the bet but leaves the money balance unchanged for the player when the given amount is zero when the player is in the dealt state.`` () =
    let random = Random()
    let player = 
        Player.Create() 
        |> Player.Deal random
    let amount = 0.0
    let expected = 
        match player.State with
        | Dealt round -> {player with State = BetPlaced (amount, round)}
        | _ -> raise (System.NotImplementedException "Kaboom expected")
    let actual =
        player
        |> Player.Bet amount
    Assert.AreEqual(expected, actual)


[<Test>]
let ``Bet.It sets the bet and decreases the player's money when the given amount is positive when the player is in the dealt state.`` () =
    let random = Random()
    let player = Player.Create() |> Player.Deal random
    let amount = 1.0
    let expected = 
        match player.State with
        | Dealt round -> 
            {player with 
                State = BetPlaced (amount, round)
                Money = player.Money - amount}
        | _ -> raise (System.NotImplementedException "Kaboom expected")
    let actual =
        player
        |> Player.Bet amount
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Bet.It sets the bet to all of the player's money and decreases the player's money to zero when the given amount is greater than or equal to the player's money when the player is in the dealt state.`` () =
    let random = Random()
    let player = Player.Create() |> Player.Deal random
    let amount = player.Money + 1.0
    let expected = 
        match player.State with
        | Dealt round -> 
            {player with 
                State = BetPlaced (player.Money, round)
                Money = 0.0}
        | _ -> raise (System.NotImplementedException "Kaboom expected")
    let actual =
        player
        |> Player.Bet amount
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Resolve.It does nothing when the player is in the resolved state.`` () =
    let player = Player.Create()
    let expected = player
    let actual =
        player
        |> Player.Resolve
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Resolve.It does nothing when the player is in the dealt state.`` () =
    let random = Random()
    let player = Player.Create() |> Player.Deal random
    let expected = player
    let actual =
        player
        |> Player.Resolve
    Assert.AreEqual(expected, actual)

[<Test>]
let ``Resolve.It adds bet to money when the round is a winner and sets state to resolved.`` () =
    let random = Random(995)
    let amount = 1.0
    let player = Player.Create() |> Player.Deal random |> Player.Bet amount
    let expected = 
        {player with 
            Money = player.Money + (amount * 2.0)
            State = Resolved}
    let actual =
        player
        |> Player.Resolve
    Assert.AreEqual(expected, actual)


[<Test>]
let ``Resolve.It sets state to resolved when the round is a not a winner.`` () =
    let random = Random(999)
    let amount = 1.0
    let player = Player.Create() |> Player.Deal random |> Player.Bet amount
    let expected = 
        {player with 
            Money = player.Money
            State = Resolved}
    let actual =
        player
        |> Player.Resolve
    Assert.AreEqual(expected, actual)
