module DeckTests

open NUnit.Framework
open AceyDucey
open System

[<Test>]
let ``Create.It creates a fresh deck of cards.`` () =
    let expectedCardCount = 52
    let actual: Card list = Deck.Create()
    let actualCardCount = actual.Length
    Assert.AreEqual(expectedCardCount, actualCardCount)

[<Test>]
let ``Create.It has no duplicated cards.`` () =
    let input = Deck.Create()
    let expectedUniqueCardCount = 52
    let actualUniqueCardCount =
        input
        |> Set.ofList
        |> Set.count
    Assert.AreEqual(expectedUniqueCardCount, actualUniqueCardCount)

[<Test>]
let ``Shuffle.It shuffles a deck of cards.`` () =
    let deck = Deck.Create()
    let random = Random(999)
    let expectedFirstCard : Card = Nine, Clubs
    let actualDeck =
        deck
        |> Deck.Shuffle random
    let actualFirstCard =
        actualDeck
        |> List.head
    Assert.AreEqual(expectedFirstCard, actualFirstCard)
    Assert.AreEqual(deck |> Set.ofList |> Set.count, actualDeck |> Set.ofList |> Set.count)
    Assert.AreEqual(deck |> List.length, actualDeck |> List.length)
    
[<Test>]
let ``Deal.It deals a new round.`` () =
    let random = Random()
    let shuffledDeck =
        Deck.Create()
        |> Deck.Shuffle random
    let expectedRound =
        {
            FirstCard = shuffledDeck.Head
            SecondCard = shuffledDeck.Tail.Head
            ThirdCard = shuffledDeck.Tail.Tail.Head
        }
    let expectedRemaining =
        shuffledDeck.Tail.Tail.Tail
    let actualRound, remainingDeck =
        shuffledDeck
        |> Deck.DealRound
    Assert.AreEqual(expectedRound, actualRound)
    Assert.AreEqual(expectedRemaining, remainingDeck)