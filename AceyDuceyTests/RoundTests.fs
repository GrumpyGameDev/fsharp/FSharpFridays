﻿module RoundTests

open NUnit.Framework
open AceyDucey
open System

let ``IsWinner.Returns true when third card is between first and second card when first card is less than second card.`` () =
    let round =
        {
            FirstCard = (Deuce, Diamonds)
            SecondCard = (King, Diamonds)
            ThirdCard = (Ten, Diamonds)
        }
    let expected = true
    let actual =
        round
        |> Round.IsWinner
    Assert.AreEqual(expected, actual)

let ``IsWinner.Returns true when third card is between first and second card when second card is less than first card.`` () =
    let round =
        {
            FirstCard = (King, Diamonds)
            SecondCard = (Deuce, Diamonds)
            ThirdCard = (Ten, Diamonds)
        }
    let expected = true
    let actual =
        round
        |> Round.IsWinner
    Assert.AreEqual(expected, actual)
