﻿module CardFaceTests

open NUnit.Framework
open AceyDucey
open System

[<Test>]
let ``GetValue.It returns the numeric equivalent of a card's face.`` () =
    [
        (Deuce,  2)
        (Three,  3)
        (Four,   4)
        (Five,   5)
        (Six,    6)
        (Seven,  7)
        (Eight,  8)
        (Nine,   9)
        (Ten,   10)
        (Jack,  11)
        (Queen, 12)
        (King,  13)
        (Ace,   14)
    ]
    |> List.iter
        (fun (cardFace, expectedValue) ->
            let actualValue =
                cardFace
                |> CardFace.GetValue
            Assert.AreEqual(expectedValue, actualValue))

[<Test>]
let ``FIRST|SECOND|NEITHER. It returns FIRST when the first card is lower.`` () =
    let first = Deuce
    let second = Three
    match (first, second) with
    | FIRST ->
        Assert.Pass()
    | _ ->
        Assert.Fail()

[<Test>]
let ``FIRST|SECOND|NEITHER. It returns SECOND when the second card is lower.`` () =
    let first = Three
    let second = Deuce
    match (first, second) with
    | SECOND ->
        Assert.Pass()
    | _ ->
        Assert.Fail()

[<Test>]
let ``FIRST|SECOND|NEITHER. It returns NEITHER when the card are the same.`` () =
    let first = Three
    let second = Three
    match (first, second) with
    | NEITHER ->
        Assert.Pass()
    | _ ->
        Assert.Fail()
