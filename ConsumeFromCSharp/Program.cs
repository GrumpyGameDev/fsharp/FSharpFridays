﻿using System;
using AceyDucey;

namespace ConsumeFromCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            CardFace face = CardFace.Ace;
            CardSuit suit = CardSuit.Spades;
            var shuffledDeck = DeckModule.Shuffle(new Random(), DeckModule.Create());
            var (round, remainingDeck) = DeckModule.DealRound(shuffledDeck);
            Console.ReadLine();
        }
    }
}
